require 'gitlab'
require_relative 'modules'
require_relative 'issuemeta'
require 'dotenv/load'
require 'docopt'
require 'date'
require 'pp'

PER_PAGE=10

docstring = <<DOCSTRING
This report will give you a count of issues and the number of issues with MRs for the given milestone.  Currently this is hard-coded to 'devops::enablement'

Usage:
  #{__FILE__} --milestone="13.1" --labels=<"devops::enablement">
  #{__FILE__} -h | --help

Options:
  -h --help                 Show this screen.
  --milestone The milestone to filter the number of issues and mr counts
  --labels The label string to include as the filter
DOCSTRING

options = Docopt::docopt(docstring)
labels = options.fetch('--labels')
milestone = (options.fetch('--milestone'))

def logger
    logger = Logger.new(STDOUT)
    logger.level = Logger::INFO
    logger
  end

Gitlab.configure do |config|
    config.endpoint = 'https://gitlab.com/api/v4'
    config.private_token = ENV["PAT"]
end

issues =  Gitlab.issues_by_milestone(milestone, labels)

issues_hash = Hash.new()

logger.info("Counting issues and MRs.  This can take a little time depending on your search criteria")
issues.each do |issue|
    group_labels = issue.labels.grep /group/

    related_merge_request_count = Gitlab.related_merge_requests(issue).length

    group_labels.each do |group_label|
        h = issues_hash[group_label] ||= IssueMeta.new(group_label)
        h.issue_count += 1
        h.mr_count += 1 if issue.merge_requests_count > 0 || related_merge_request_count > 0
    end
end
p "Total issue count for '#{labels}': #{issues.length}"
issues_hash.each do |key, value|
    p value.to_s
end

=begin
p "Memory Count #{memory} with MR #{memory_with_mr}"
p "Geo Count #{geo} with MR #{geo_with_mr}"
p "Distribution Count #{distribution} with MR #{distribution_with_mr}"
p "Search Count #{search} with MR #{search_with_mr}"
p "Database Count #{database} with MR #{database_with_mr}"
=end
p "Done"