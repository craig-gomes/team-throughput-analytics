require 'gitlab'
require_relative 'modules'
require 'dotenv/load'
require 'docopt'
require 'date'
require 'pp'
require 'squid' #graphing gem
require 'logger'

def logger
    logger = Logger.new(STDOUT)
    logger.level = Logger::WARN
    logger
  end

docstring = <<DOCSTRING
Create a graph comparing MR and Comment activity over a given period.

Usage:
  #{__FILE__} --user_ids=<userid1,userid2,userid3> --before="2019-10-31" --after="2019-10-01"
  #{__FILE__} -h | --help

Options:
  -h --help                 Show this screen.
  --epic=<epic1,epic2>      List the epics you would like to display in burn up charts
DOCSTRING

PER_PAGE = 100
Gitlab.configure do |config|
    config.endpoint = 'https://gitlab.com/api/v4'
    config.private_token = ENV["PAT"]
end

def graph_line(graph_data, title)
    Prawn::Document.generate title do
      data = graph_data
         chart data, type: :line, labels: [true, true, true], line_widths: [2, 2], colors: %w(fc6d26 6e49cb), format: :date
         #chart data, labels: [true, true], colors: %w(fc6d26 6e49cb), format: :date
    end
  end

options = Docopt::docopt(docstring)
user_ids = options.fetch('--user_ids').split(',')
before = options.fetch('--before')
after = options.fetch('--after')

#create hash with YYYY-MM-DD as key to count comments and MRs for that day
=begin
comment_hash =  Hash[(after..before).collect { |day| [day, 0]}]
mr_hash = comment_hash.clone
=end

after_date = Date.parse(after)
before_date = Date.parse(before)

if (before_date - after_date) > 30
    puts "Graphing doesn't work well beyond 30 data points.  Try reducing the time between before and after"
    return
end

comment_hash = Hash[(after_date...(before_date+1)).to_a.collect { |day| [day.to_s,0]}]
mr_hash = comment_hash.clone
commit_hash = comment_hash.clone


user_ids.each do |user_id|
    logger.info("user_id: #{user_id}")
    comments =  Gitlab.user_comments(user_id, before, after)
    mrs = Gitlab.user_mrs(user_id, Date.parse(after), Date.parse(before))

    comments.each do |comment|
        logger.info("comment.created_at #{comment.created_at[0,10]}")
        comment_hash[comment.created_at[0,10]] +=1
    end

    mrs.each do |mr|
        begin
            unless mr.nil?
                logger.info("mr.merged_at #{mr.merged_at[0,10]}")
                mr_hash[mr.merged_at[0,10]] +=1
            end
            commits = Gitlab.mr_commits(mr.project_id, mr.iid)
            logger.info("commits.size #{commits.size}")
            commits.each do |commit|
              commit_hash[commit.created_at[0,10]] +=1
            end
        rescue => err
            logger.fatal("Caught exception; exiting")
            logger.fatal(err)
        end

    end
end




data = {Comments: comment_hash, MRs: mr_hash, Commits: commit_hash} 

graph_line(data, "Comments between #{after} and #{before}.pdf")
