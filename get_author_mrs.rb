require 'gitlab'
require_relative 'modules'
require 'dotenv/load'
require 'docopt'
require 'date'
require 'pp'

PER_PAGE=20

docstring = <<DOCSTRING
Create Issue and Merge Request burn up charts for the given epics.

Usage:
  #{__FILE__} --author_id=<id1,id2,id3> --begin=YYYY-MM-DD --end=YYYY-MM-DD
  #{__FILE__} --author_id=<id1,id2,id3> --days=#
  #{__FILE__} -h | --help

Options:
  -h --help                 Show this screen.
  --epic=<epic1,epic2>      List the epics you would like to display in burn up charts
DOCSTRING


Gitlab.configure do |config|
    config.endpoint = 'https://gitlab.com/api/v4'
    config.private_token = ENV["PAT"]
end

#12452 = ayufan
#4087087 = alipniagov
#4098809 = qingyu
#4787364 = Nikola
#4713264 = matthias
#1562869 = andreas
#5297955 = pbair
#1982738 = yannis

#example id retrieval https://gitlab.com/api/v4/users?username=iroussos


#example command ruby get_author_mrs.rb --author_id=12452,4087087,4098809,4787364,4713264,1562869 --begin=2019-10-01 --end=2019-10-30 
#db team ruby get_author_mrs.rb --author_id=1982738,5297955,1562869 --begin=2020-05-16 --end=2020-05-26

options = Docopt::docopt(docstring)
author_ids = options.fetch('--author_id').split(',')
days = options.fetch('--days').to_i
if days > 1
  end_date = Date.today
  begin_date = Date.today - days
else
  begin_date = Date.parse(options.fetch('--begin'))
  end_date = Date.parse(options.fetch('--end'))
end

all_mrs = []

def get_mr_urls(all_mrs)
  urls = File.open("urls.csv", "w")
  urls.puts "USERNAME|WEB_URL|TITLE"
  all_mrs.each do |mr|
    urls.puts "#{mr.author.username} | #{mr.web_url} | #{mr.title}"
  end
end

author_ids.each do |author_id|
    all_mrs = all_mrs | Gitlab.user_mrs(author_id, begin_date, end_date)
end

get_mr_urls(all_mrs)

puts "Total count of MRs for the period between #{begin_date} and #{end_date} is #{all_mrs.count}"