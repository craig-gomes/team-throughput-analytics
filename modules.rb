require 'date'

class Gitlab::Client
  
      module MergeRequests
        def related(project_id, issue_id)
          should_continue=true
          all_mrs=[]
  
          (1..100).each do |current_page|
            if !should_continue
              break
            end
  
            mrs = get("/projects/#{project_id}/issues/#{issue_id}/related_merge_requests", {per_page:PER_PAGE})
            
            if mrs.size < PER_PAGE
              should_continue=false
            end
  
            all_mrs += mrs
          end
          all_mrs
          
        end

        def user_mrs(author_id, begin_date, end_date)
          should_continue = true
          all_mrs = []

          (1..10).each do |current_page|
            if !should_continue
              break
            end

            mrs = get("/merge_requests?author_id=#{author_id}&scope=all&per_page=#{PER_PAGE}&page=#{current_page}")

            mrs.each do |mr|
              unless mr.merged_at.nil?
                merged_at = Date.parse(mr.merged_at)
                if merged_at.between?(begin_date, end_date)
                  all_mrs << mr
                elsif merged_at < begin_date
                  should_continue=false
                  break
                end
              end
            end
          end
          all_mrs
        end

        def get_all(get_url)
          should_continue=true
          all=[]
  
          (1..100).each do |current_page|
            if !should_continue
              break
            end
  
            page = get(get_url+"?per_page=#{PER_PAGE}&page=#{current_page}")
            
            if all.size < PER_PAGE
              should_continue=false
            end
  
            all +=page
          end
          all
        end
        
        def mr_commits(project_id, mr_id)
          get_all("/projects/#{project_id}/merge_requests/#{mr_id}/commits")
        end
      end

      module Issues

        def user_comments(user_id, before, after)
          should_continue = true
          all_comments = []
              
          (1..100).each do |current_page|
            logger.info("user_id: #{user_id} page: #{current_page}")
            logger.info("/users/#{user_id}/events?action=commented&after=#{after}&before=#{before}&per_page=#{PER_PAGE}&page=#{current_page}")
            if !should_continue
              logger.info("should not continue")
              break
            end
            next_comments =  get("/users/#{user_id}/events?action=commented&after=#{after}&before=#{before}&per_page=#{PER_PAGE}&page=#{current_page}")
            logger.info("next_comments size #{next_comments.size}")
            all_comments = all_comments | next_comments
            logger.info("all_comments size #{all_comments.size}")
            if next_comments.size < PER_PAGE
              should_continue = false
            end
          end
          all_comments
        end

        def issues_by_milestone(milestone, labels)
          all_issues = [] 
          should_continue = true

          (1..100).each do |current_page|
            if !should_continue
              break
            end
            next_issues = get("/issues?milestone=#{milestone}&labels=#{labels}&per_page=#{PER_PAGE}&page=#{current_page}&scope=all")
            all_issues = all_issues | next_issues
            if next_issues.size < PER_PAGE
              should_continue = false
            end
            logger.info("current_page=#{current_page} per_page=#{PER_PAGE} next_issues.size=#{next_issues.size}")
          end
          all_issues
        end

        def related_merge_requests(issue)
          get("/projects/#{issue.project_id}/issues/#{issue.iid}/related_merge_requests")
        end
      end
    end

    
    module Gitlab
      class Client < API
        include MergeRequests
        include Issues
      end
    end
   