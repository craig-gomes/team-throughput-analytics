# Team throughput analytics

Create a .env file with the following content

PAT='your_personal_access_token'

## get_user_comments.rb

The following command will create a PDF in your local folder with comments and MRs for the time period provided
ruby get_user_comments.rb --user_ids="4087087,4098809,4787364,4713264" --before="2019-10-31" --after="2019-10-01"

## get_author_mrs.rb

The following command will create urls.csv with the user name, url and title of the MRs created within the dates indicated by the `before` and `after` command line parameters

## get_issue_counts_by_group_label.rb

The following command will count the total number of issues and issues with MRs based on the milestone and labels provided.  The totals are then summarized by group.  For example, this command: `ruby get_issue_counts_by_group_label.rb --milestone="13.1" --labels="devops::enablement"`  will yield a result similar to:

> "Total issue count for 'devops::enablement': 230"</br>
> "Group label: 'group::geo' Issue count: 65 MR count: 46"</br>
> "Group label: 'group::memory' Issue count: 21 MR count: 15"</br>
> "Group label: 'group::distribution' Issue count: 76 MR count: 64"</br>
> "Group label: 'group::database' Issue count: 28 MR count: 20"</br>
> "Group label: 'group::global search' Issue count: 37 MR count: 34"</br>
> "Group label: 'group::ecosystem' Issue count: 1 MR count: 1"</br>
