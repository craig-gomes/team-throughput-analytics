class IssueMeta

    attr_accessor :mr_count, :issue_count 
   
    def initialize(group_label)
        @group_label = group_label
        @mr_count = 0
        @issue_count = 0
    end

    def to_s
        "Group label: '#{@group_label}' Issue count: #{@issue_count} MR count: #{@mr_count}"
    end
end

